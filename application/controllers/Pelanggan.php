<?php
defined('BASEPATH') or exit('No direct script access allowed');

class pelanggan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('logged') != TRUE) {
            redirect('login');
        }

        $this->load->model('pelanggan_model', 'pelanggan');
        date_default_timezone_set('Asia/Jakarta');
    }

    public function index()
    {
        $data = [
            'account' => $this->db->get_where('tb_users', ['user_id' => $this->session->userdata('user_id')])->row(),
            'title' => 'Pelanggan',
            'result' => $this->pelanggan->getData(),
        ];

        $this->load->view('layout/head', $data);
        $this->load->view('layout/header', $data);
        $this->load->view('layout/sidebar');
        $this->load->view('pelanggan/index', $data);
        $this->load->view('layout/footer');
    }

    public function create()
    {
        $this->form_validation->set_rules('nama_pelanggan', 'nama_pelanggan', 'required');
        $this->form_validation->set_rules('nama_pengirim', 'nama_pengirim', 'required');
        $this->form_validation->set_rules('nama_penerima', 'nama_penerima', 'required');

        if ($this->form_validation->run() == false) {
            $data = [
                'account' => $this->db->get_where('tb_users', ['user_id' => $this->session->userdata('user_id')])->row(),
                'title' => 'Pelanggan'
            ];

            $this->load->view('layout/head', $data);
            $this->load->view('layout/header', $data);
            $this->load->view('layout/sidebar');
            $this->load->view('pelanggan/create');
            $this->load->view('layout/footer');
        } else {
            $this->pelanggan->insertData();
            $this->session->set_flashdata('success', true);
            $this->session->set_flashdata('message', '<strong>Berhasil!</strong> Data anda telah tersimpan.');
            redirect('pelanggan');
        }
    }

    public function edit($id)
    {
        $this->form_validation->set_rules('nama_pelanggan', 'nama_pelanggan', 'required');
        $this->form_validation->set_rules('nama_pengirim', 'nama_pengirim', 'required');
        $this->form_validation->set_rules('nama_penerima', 'nama_penerima', 'required');


        if ($this->form_validation->run() == false) {
            $data = [
                'account' => $this->db->get_where('tb_users', ['user_id' => $this->session->userdata('user_id')])->row(),
                'title' => 'Pelanggan',
                'res' => $this->pelanggan->getDataByID($id)
            ];

            $this->load->view('layout/head', $data);
            $this->load->view('layout/header', $data);
            $this->load->view('layout/sidebar');
            $this->load->view('pelanggan/edit', $data);
            $this->load->view('layout/footer');
        } else {
            $this->pelanggan->updateData($id);
            $this->session->set_flashdata('success', true);
            $this->session->set_flashdata('message', '<strong>Berhasil!</strong> Data anda telah diperbaharui.');
            redirect('pelanggan');
        }
    }

    public function delete($id)
    {
        $this->pelanggan->deleteData($id);
        $this->session->set_flashdata('success', true);
        $this->session->set_flashdata('message', '<strong>Berhasil!</strong> Data anda telah dihapuskan.');
        redirect('pelanggan');
    }
}
