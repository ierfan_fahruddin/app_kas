<main id="main" class="main">

    <div class="pagetitle">
        <h1><?= $title ?></h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                <li class="breadcrumb-item active"><?= $title ?></li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">
        <div class="card">

            <div class="card-body">
                <h5 class="card-title d-flex justify-content-between">
                    Tambah Pelanggan
                    <button type="button" class="btn btn-warning btn-sm" onclick="history.back()">
                        <i class="bi bi-skip-backward-circle"></i> Kembali
                    </button>
                </h5>
                <form action="<?= base_url() ?>pelanggan/create" method="post">
                    <div class="row mb-3">
                        <label for="nama_pelanggan" class="col-sm-2 col-form-label">Nama Pelanggan</label>
                        <div class="col-sm-10">
                            <input type="text" name="nama_pelanggan" id="nama_pelanggan" class="form-control"
                                value="<?= set_value('nama_pelanggan') ?>">
                            <small class="form-text text-danger"><?= form_error('nama_pelanggan') ?></small>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="nama_pengirim" class="col-sm-2 col-form-label">Nama Pengirim</label>
                        <div class="col-sm-10">
                            <input type="text" name="nama_pengirim" id="nama_pengirim" class="form-control"
                                autocomplete="off" value="<?= set_value('nama_pengirim') ?>">
                            <small class="form-text text-danger"><?= form_error('nama_pengirim') ?></small>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="nama_penerima" class="col-sm-2 col-form-label">Nama Penerima</label>
                        <div class="col-sm-10">

                            <input type="text" name="nama_penerima" id="nama_penerima" class="form-control"
                                autocomplete="off" value="<?= set_value('nama_penerima') ?>">
                            <small class="form-text text-danger"><?= form_error('nama_penerima') ?></small>
                        </div>
                    </div>
                    <div class="text-end">
                        <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </section>

</main>