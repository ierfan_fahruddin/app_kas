<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cetak</title>
    <link href="<?= base_url() ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <div class="container">
        <h2 class="text-center">Laporan Rekapitulasi</h2>
        <div class="row">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Tanggal</th>
                        <th scope="col">Keterangan</th>
                        <th scope="col">Pemasukan</th>
                        <th scope="col">Pengeluaran</th>
                        <th scope="col">Saldo</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1; ?>
                    <?php $saldo = 0 ?>
                    <?php foreach ($result as $res) : ?>
                    <tr>
                        <th scope="row"><?= $no++ ?></th>
                        <td><?= $res->tanggal ?></td>
                        <td><?= $res->keterangan ?></td>
                        <td>Rp <?= number_format($res->pemasukan, 0, ",", ".")  ?></td>
                        <td>Rp <?= number_format($res->pengeluaran, 0, ",", ".")  ?></td>
                        <td>
                            <?php
                                if ($res->pemasukan != null) {
                                    $saldo += $res->pemasukan;
                                } else {
                                    $saldo -= $res->pengeluaran;
                                }
                                ?>

                            Rp <?= number_format($saldo, 0, ",", ".")  ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                    <tr>
                        <td>Total</td>
                        <td></td>
                        <td></td>
                        <td>Rp <?= number_format($sumMasuk->pemasukan, 0, ",", ".") ?></td>
                        <td>Rp <?= number_format($sumKeluar->pengeluaran, 0, ",", ".") ?></td>
                        <td>Rp <?= number_format(($sumMasuk->pemasukan - $sumKeluar->pengeluaran), 0, ",", ".") ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <script>
    window.addEventListener('load', function() {
        setTimeout(function() {
            window.print();
        }, 500);
        setTimeout(function() {
            location.href = "<?= base_url('report') ?>";
        }, 2000);
    });
    </script>
</body>

</html>