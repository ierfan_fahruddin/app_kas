<?php
class user_model extends CI_Model
{
    public function getData()
    {
        return $this->db->select('*')->from('tb_users')->order_by('user_id', 'DESC')->get()->result();
    }

    public function sumData()
    {
        return $this->db->select_sum('pengeluaran')->from('tb_user')->get()->row();
    }

    public function getDataByID($id)
    {
        return $this->db->get_where('tb_users', ['user_id' => $id])->row();
    }

    public function insertData()
    {
        $data = [
            'nama_lengkap' => htmlspecialchars($this->input->post('nama_lengkap')),
            'username' => htmlspecialchars($this->input->post('username')),
            'password' => password_hash(htmlspecialchars($this->input->post('password')), PASSWORD_DEFAULT),
        ];

        $this->db->insert('tb_users', $data);
    }

    public function updateData($id)
    {
        $user = $this->db->query('SELECT * FROM tb_users where user_id =' . $id)->row_array();
        if ($this->input->post('password') == $user['password']) {
            $data = [
                'nama_lengkap' => htmlspecialchars($this->input->post('nama_lengkap')),
                'username' => htmlspecialchars($this->input->post('username')),
            ];
        } else {
            $data = [
                'nama_lengkap' => htmlspecialchars($this->input->post('nama_lengkap')),
                'username' => htmlspecialchars($this->input->post('username')),
                'password' => htmlspecialchars($this->input->post('password')),
            ];
        }


        $this->db->where('user_id', $id)->update('tb_users', $data);
    }

    public function deleteData($id)
    {
        $this->db->where('user_id', $id)->delete('tb_users');
    }
}
