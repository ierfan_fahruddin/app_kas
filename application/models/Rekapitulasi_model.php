<?php
class Rekapitulasi_model extends CI_Model
{
    public function getData()
    {
        return $this->db->select('*')->from('tb_kas')->order_by('tanggal', 'DESC')->get()->result();
    }

    public function sumData()
    {
        return $this->db->select_sum('pemasukan')->from('tb_kas')->get()->row();
    }
    function getDataCetak()
    {
        $startDate = $this->input->get('start');

        if (empty($startDate)) {
            $result = $this->db->select('*')
                ->from('tb_kas')
                ->order_by('tanggal', 'DESC')
                ->get()
                ->result();
        } else {
            $startDate = $this->input->get('start');
            $endDate = $this->input->get('end');

            if (empty($startDate)) {
                $startDate = date('Y-m-d');
            }

            if (empty($endDate)) {
                $endDate = date('Y-m-d');
            }

            $this->db->select('*')
                ->from('tb_kas')
                ->where('tanggal >=', $startDate)
                ->where('tanggal <=', $endDate);

            $result = $this->db->get()->result();
        }


        return $result;
    }
}
