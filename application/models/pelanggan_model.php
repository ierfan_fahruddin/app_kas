<?php
class pelanggan_model extends CI_Model
{
    public function getData()
    {
        return $this->db->select('*')->from('tb_pelanggan')->order_by('id', 'DESC')->get()->result();
    }
    public function getData1()
    {
        return $this->db->select('*')->from('tb_pelanggan')->order_by('id', 'DESC')->get()->result_array();
    }


    public function getDataByID($id)
    {
        return $this->db->get_where('tb_pelanggan', ['id' => $id])->row();
    }

    public function insertData()
    {
        $data = [
            'nama_pelanggan' => htmlspecialchars($this->input->post('nama_pelanggan')),
            'nama_pengirim' => htmlspecialchars($this->input->post('nama_pengirim')),
            'nama_penerima' => htmlspecialchars($this->input->post('nama_penerima')),
        ];

        $this->db->insert('tb_pelanggan', $data);
    }

    public function updateData($id)
    {
        $data = [
            'nama_pelanggan' => htmlspecialchars($this->input->post('nama_pelanggan')),
            'nama_pengirim' => htmlspecialchars($this->input->post('nama_pengirim')),
            'nama_penerima' => htmlspecialchars($this->input->post('nama_penerima')),
        ];

        $this->db->where('id', $id)->update('tb_pelanggan', $data);
    }

    public function deleteData($id)
    {
        $this->db->where('id', $id)->delete('tb_pelanggan');
    }
}
